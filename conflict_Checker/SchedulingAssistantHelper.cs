﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using System.Linq;

namespace conflict_Checker
{
    public class SchedulingAssistantHelper
    {
        Document document = new Document();
        PdfWriter writer;
        BaseFont bfntHead;
        Font fntHead;
        System.IO.FileStream fs;
        public SchedulingAssistantHelper(String strPdfPath)
        {
            fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);

            document.SetPageSize(iTextSharp.text.PageSize.A4);
            writer = PdfWriter.GetInstance(document, fs);
            document.Open();


        }

        public List<int> CheckConflict(DataTable dt, List<int> illegalRowsList)
        {
            List<int> rowList = new List<int>();

            for (int currentRow = 0; currentRow < dt.Rows.Count; currentRow++)
            {
                DataRow rowValue = dt.Rows[currentRow];
                // string s = rowValue["Days"].ToString();
                for (int otherRow = 0; otherRow < dt.Rows.Count; otherRow++)
                {
                    DataRow otherRowValues = dt.Rows[otherRow];
                    if (rowValue != otherRowValues && !illegalRowsList.Contains(currentRow) && !illegalRowsList.Contains(otherRow))
                    {
                        String[] weekDay = new String[] { "M", "T", "W", "R", "F" };
                        foreach (var day in weekDay)
                        {
                            if (rowValue["DAYS"].ToString().Contains(day) && otherRowValues["DAYS"].ToString().Contains(day))
                            {
                                if (IsTimeOverLapping(rowValue["TIME"].ToString(), otherRowValues["TIME"].ToString()))
                                {
                                    if (rowValue["LOCATION"].ToString() == otherRowValues["LOCATION"].ToString() || rowValue["INSTRUCTOR_ID"].ToString() == otherRowValues["INSTRUCTOR_ID"].ToString() || (rowValue["SECTION"].ToString() + rowValue["CRSE#"].ToString()) == (otherRowValues["SECTION"].ToString() + otherRowValues["CRSE#"].ToString()))
                                    {
                                        if (!rowList.Contains(currentRow))
                                        {
                                            rowList.Add(currentRow);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return rowList;
        }

        public bool IsTimeOverLapping(String time1, String time2)
        {
            DateTime startTime1 = Convert.ToDateTime(time1.Split('-')[0].Insert(2, ":"));
            DateTime endtime1 = Convert.ToDateTime(time1.Split('-')[1].Insert(2, ":"));
            DateTime startTime2 = Convert.ToDateTime(time2.Split('-')[0].Insert(2, ":"));
            DateTime endtime2 = Convert.ToDateTime(time2.Split('-')[1].Insert(2, ":"));

            bool overlap = startTime1 <= endtime2 && startTime2 <= endtime1;
            return overlap;
        }

        public bool AreAllColumnsEmpty(DataRow dr)
        {
            bool isEmpty = false;
            for (int i = 0; i < dr.ItemArray.Length; i++)
            {
                if (dr.ItemArray[i].ToString() == "{}" || dr.ItemArray[i] == null || dr.ItemArray[i].ToString() == string.Empty)
                {
                    isEmpty = true;
                }
                else
                {
                    isEmpty = false;
                    return isEmpty;
                }
            }
            return isEmpty;
        }

        public bool IsColumnEmpty(DataTable dt)
        {
            bool isEmpty = false;
            bool flag = false;
            for (int col = 0; col < dt.Columns.Count; col++)
            {
                for (int row = 0; row < dt.Rows.Count; row++)
                {
                    if (dt.Rows[row][col].ToString() == "{}" || dt.Rows[row][col] == null || dt.Rows[row][col].ToString() == string.Empty)
                    {
                        isEmpty = true;
                    }
                    else
                    {
                        isEmpty = false;
                        break;
                    }
                }
                if (isEmpty == true)
                {
                    dt.Columns.Remove(dt.Columns[col]);
                    flag = true;
                }
            }
            return flag;
        }

        public List<cellIndex> emptyCellList(DataTable dt)
        {
            List<cellIndex> index = new List<cellIndex>();

            for (int c = 0; c < dt.Columns.Count; c++)
            {
                for (int r = 0; r < dt.Rows.Count; r++)
                {
                    if (isCellEmpty(dt, r, c))
                    {
                        index.Add(new cellIndex() { row = r, column = c });
                    }
                }

            }
            return index;
        }

        public bool isCellEmpty(DataTable dt, int row, int column)
        {
            bool isCellEmpty = false;
            if (dt.Rows[row][column].ToString() == "{}" || dt.Rows[row][column] == null || dt.Rows[row][column].ToString() == string.Empty)
            {
                isCellEmpty = true;
                return isCellEmpty;
            }
            return isCellEmpty;
        }

        public bool matchString(String reg, String text)
        {
            Regex r = new Regex(reg);
            Match m = r.Match(text);
            if (m.Success)
            {
                return true;
            }
            return false;
        }

        public List<int> illegalFormatRowList(DataTable dt)
        {
            List<int> rowList = new List<int>();
            for (int r = 0; r < dt.Rows.Count; r++)
            {
                if (!matchString(@"^\d{4}-\d{4}$", dt.Rows[r]["TIME"].ToString()) || !matchString(@"M|T|W|R|F|MT|MW|MR|MF|TW|TR|TF|WR|WF|RF|MTW|MTR|MTF|MWR|MWF|MRF|TWR|TWF|TRF|WRF", dt.Rows[r]["DAYS"].ToString()))
                {
                    if (!rowList.Contains(r))
                    {
                        rowList.Add(r);
                    }
                }
                DataRow rowValue = dt.Rows[r];
                if (isCellEmpty(dt, r, rowValue.Table.Columns["INSTRUCTOR_ID"].Ordinal))
                {
                    if (!rowList.Contains(r))
                    {
                        rowList.Add(r);
                    }
                }
                if (isCellEmpty(dt, r, rowValue.Table.Columns["LOCATION"].Ordinal))
                {
                    if (!rowList.Contains(r))
                    {
                        rowList.Add(r);
                    }
                }
                if (isCellEmpty(dt, r, rowValue.Table.Columns["INSTRUCTOR NAME"].Ordinal))
                {
                    if (!rowList.Contains(r))
                    {
                        rowList.Add(r);
                    }
                }
            }
            return rowList;
        }

        public void ExportDataTableToPdf(DataTable dtblTable, string strHeader)
        {
            try
            {

                //Report Header
                bfntHead = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                fntHead = new Font(bfntHead, 16, 1, BaseColor.GRAY);
                Paragraph prgHeading = new Paragraph();
                prgHeading.Alignment = Element.ALIGN_CENTER;
                prgHeading.Add(new Chunk(strHeader.ToUpper(), fntHead));
                document.Add(prgHeading);

                //Author
                Paragraph prgAuthor = new Paragraph();
                BaseFont btnAuthor = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                Font fntAuthor = new Font(btnAuthor, 8, 2, BaseColor.GRAY);
                prgAuthor.Alignment = Element.ALIGN_RIGHT;
                prgAuthor.Add(new Chunk("Author : Scheduling Assistant", fntAuthor));
                prgAuthor.Add(new Chunk("\nRun Date : " + DateTime.Now.ToShortDateString(), fntAuthor));
                document.Add(prgAuthor);

                //Add a line seperation
                Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                document.Add(p);

                //Add line break
                document.Add(new Chunk("\n", fntHead));

                //Write the table
                PdfPTable table = new PdfPTable(dtblTable.Columns.Count);
                //Table header
                BaseFont btnColumnHeader = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                Font fntColumnHeader = new Font(btnColumnHeader, 10, 1, BaseColor.WHITE);
                for (int i = 0; i < dtblTable.Columns.Count; i++)
                {
                    PdfPCell cell = new PdfPCell();
                    cell.BackgroundColor = BaseColor.GRAY;
                    cell.AddElement(new Chunk(dtblTable.Columns[i].ColumnName.ToUpper(), fntColumnHeader));
                    table.AddCell(cell);
                }
                //table Data
                for (int i = 0; i < dtblTable.Rows.Count; i++)
                {
                    for (int j = 0; j < dtblTable.Columns.Count; j++)
                    {
                        table.AddCell(dtblTable.Rows[i][j].ToString());
                    }
                }
                document.Add(table);
                document.NewPage();

            }
            catch (Exception)
            {
                throw;
            }

        }

        public void MakeDataTable(DataTable dt, String type, List<int> infectedRowsList, List<string> distinctValues)
        {

            DataTable newDT = new DataTable();
            String excelValue = string.Empty;
            switch (type)
            {
                case "Room":
                    newDT.Columns.Add("LOCATION");
                    newDT.Columns.Add("Day-Time");
                    foreach (var item in distinctValues)
                    {
                        var crn = dt.AsEnumerable().Where(x => Convert.ToString(x.Field<string>("LOCATION")) == item).Select(t => t.Field<string>("Days") + "-" + t.Field<string>("TIME")).ToList();
                        newDT.Rows.Add(item, string.Join(" ", crn));
                        //for (int currentRow = 0; currentRow < dt.Rows.Count; currentRow++)
                        //{
                        //    DataRow rowValue = dt.Rows[currentRow];

                        //    if (excelValue == item && !infectedRowsList.Contains(currentRow))
                        //    {
                        //        excelValue = rowValue["LOCATION"].ToString();
                        //        newDT.Rows.Add(rowValue["DAYS"].ToString(), rowValue["TIME"].ToString(), excelValue);
                        //    }
                        //}
                    }
                    ExportDataTableToPdf(newDT, excelValue);

                    break;
                case "Instructor":
                    newDT.Columns.Add("INSTRUCTOR NAME");
                    newDT.Columns.Add("CRSE");
                    newDT.Columns.Add("SECTION");

                    foreach (var item in distinctValues)
                    {

                        var crn = dt.AsEnumerable().Where(x => Convert.ToString(x.Field<double>("INSTRUCTOR_ID")) == item).Select(t => Convert.ToString(t.Field<double>("CRSE#"))).ToList();
                        var section =dt.AsEnumerable().Where(x => Convert.ToString(x.Field<double>("INSTRUCTOR_ID")) == item).Select(t => Convert.ToString(t.Field<double>("SECTION"))).ToList();
                        var InstructorName = dt.AsEnumerable().Where(x => Convert.ToString(x.Field<double>("INSTRUCTOR_ID")) == item).Select(t => t.Field<string>("INSTRUCTOR NAME")).FirstOrDefault();
                        newDT.Rows.Add(InstructorName,string.Join(",", crn) ,string.Join(",", section));
                        //for (int currentRow = 0; currentRow < dt.Rows.Count; currentRow++)
                        //{
                        //    //DataRow rowValue = dt.Rows[currentRow];
                        //   // String name = rowValue["INSTRUCTOR NAME"].ToString();
                        //    //String id = rowValue["INSTRUCTOR_ID"].ToString();
                        //   // if (id == item && !infectedRowsList.Contains(currentRow))
                        //    {
                        //        //excelValue = name + "(ID: " + id + ")";
                        //        //newDT.Rows.Add(rowValue["DAYS"].ToString(), rowValue["TIME"].ToString(), rowValue["CRN"].ToString(), rowValue["LOCATION"].ToString(), rowValue["TITLE"].ToString());
                        //       newDT.Rows.Add(InstructorName, string.Join(",",crn), title);
                        //    }
                        //}
                        
                    }
                    ExportDataTableToPdf(newDT, excelValue);
                    break;
                default:
                    newDT.Columns.Add("No Data to display");
                    newDT.Rows.Add("No data to display");
                    break;
            }
            document.Close();
            writer.Close();
            fs.Close();
        }
    }

    public class cellIndex
    {
        public int row { get; set; }
        public int column { get; set; }
    }

    public static class ExtensionMethods
    {
        public static void DoubleBuffered(this DataGridView dgv, bool setting)
        {
            Type dgvType = dgv.GetType();
            PropertyInfo pi = dgvType.GetProperty("DoubleBuffered",
                BindingFlags.Instance | BindingFlags.NonPublic);
            pi.SetValue(dgv, setting, null);
        }
    }
}
