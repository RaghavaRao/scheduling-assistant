﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Threading;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using LinqToExcel;
using LinqToExcel.Attributes;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;


namespace conflict_Checker {
    public partial class SchedulingAssistant : Form {
        static ExcelQueryFactory excelQF;       
        DataTable dataTbl = new DataTable();
        List<ChangedCellvalue> listchanged = new List<ChangedCellvalue>();       
        ChangedCellvalue[] myvalue;        
        List<Int32> rowmarkedasdeleted = new List<int>();        
        Microsoft.Office.Interop.Excel.Application excelSheet = new Microsoft.Office.Interop.Excel.Application();
        Boolean flag = false;

       /*
        *   It initializes the application
        */
        public SchedulingAssistant() {
            InitializeComponent();
            this.dataGridView.DataError += this.DataGridView1_DataError;
            MessageBox.Show("Please Save and Close all the other Excel files. This application will forcely close all other Excel files.", "Save & Close", MessageBoxButtons.OK, MessageBoxIcon.Information);
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
        }

        //Disposes the rotating icon after importing the Excel sheet
        public void DisposPI() {
            if(pictureBox3.Visible) {
                ((IDisposable)pictureBox3).Dispose();
            }
        }

        public void DisposPI1()
        {
            if (pictureBox4.Visible)
            {
                ((IDisposable)pictureBox4).Dispose();
            }
        }
		//Windows close button overridden. It kills all the Excel instances when closing the application

		protected override void OnFormClosing(System.Windows.Forms.FormClosingEventArgs e)
		{
			base.OnFormClosing(e);
			if (e.CloseReason == CloseReason.WindowsShutDown) return;
			KillExcel();
		}

		private void KillExcel()
		{
			try
			{
				Process[] allExclProc = Process.GetProcessesByName("EXCEL");
				foreach (Process allProcID in allExclProc)
				{
					int procsID = allProcID.Id;
					Process.GetProcessById(procsID).kill();
				}
			}
			catch (Exception expt)
			{

			}
			Clipboard.Clear();
			Application.Exit();

		}

        /*
         * Allows a user to choose an Exce file
         */
        private void Choose_File_Click(object sender, EventArgs e) {
            try
            {
                checkConflicts.Enabled = true;
                OpenFileDialog selectExcelFile = new OpenFileDialog();
                selectExcelFile.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";

                if (selectExcelFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.textBoxSheetPath.Text = selectExcelFile.FileName;
                    Excel.Application xlApp = new Excel.Application();
                    Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(selectExcelFile.FileName, 0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);

                    ExcelSheetNames.Items.Clear();
                    ExcelSheetNames.ResetText();
                    foreach (Excel.Worksheet worksheet in xlWorkBook.Worksheets)
                    {
                        ExcelSheetNames.Items.Add(worksheet.Name);
                    }
                    ExcelSheetNames.SelectedIndex = 0;
                    ExcelSheetNames.DropDownStyle = ComboBoxStyle.DropDownList;
                }
            }
            catch (Exception exx)
            {
                String str = exx.StackTrace;
                Console.WriteLine(str);
            }
        }
        //Importing an Excel file
        private void checkConflicts_Click(object sender, EventArgs e)
        {
            try
            {
                button1.Enabled = true;
                checkConflicts.Enabled = false;
                Delete.Enabled = true;
                Add.Enabled = true;
                Clipboard.Clear();
                clearAllToolStripMenuItem_Click(null, new EventArgs());
                pictureBox3.Visible = true;
                dataGridView.DefaultCellStyle.SelectionBackColor = Color.LightSkyBlue;
                dataGridView.DefaultCellStyle.ForeColor = Color.Black;

                if (textBoxSheetPath.Text == string.Empty || ExcelSheetNames.Text == String.Empty)
                {
                    MessageBox.Show("Please choose an Excel file", "Scheduling Assistant", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    pictureBox3.Visible = false;
                }
                else
                {
                    Clipboard.Clear();
                    clearAllToolStripMenuItem_Click(null, new EventArgs());
                    String connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + textBoxSheetPath.Text + @";Extended Properties= ""Excel 12.0;HDR=YES;IMEX=1;MAXSCANROWS=15;READONLY=FALSE""";
                    OleDbConnection connect = new OleDbConnection(connectionString);
                    OleDbCommand oconn = new OleDbCommand("select * from[" + ExcelSheetNames.Text + "$]", connect);
                    connect.Open();
                    OleDbDataAdapter data = new OleDbDataAdapter(oconn);
                    data.Fill(dataTbl);
                    dataTbl.Columns["INSTRUCTOR ID"].ColumnName = "INSTRUCTOR_ID";
                    dataGridView.DataSource = dataTbl;

                    foreach (DataGridViewRow item in dataGridView.Rows)
                    {
                        if (item.Index != -1)
                        {

                            foreach (DataGridViewCell cel in item.Cells)
                            {
                                string strSplitValue = Convert.ToString(cel.Value);
                                if (strSplitValue.Contains('\n'))
                                {
                                    cel.Value = strSplitValue.Split('\n')[0];
                                }
                            }
                        }
                    }

                    excelQF = new ExcelQueryFactory(textBoxSheetPath.Text);
                    DisposPI();
                }
            }
            catch (Exception exx)
            {
                String str = exx.StackTrace;
                Console.WriteLine(str); 
                DisposPI();
            }
        }

        private void Export_File_Click(object sender, EventArgs e) {
            try
            {
                pictureBox4.Visible = true;
                if (textBoxSheetPath.Text == string.Empty || ExcelSheetNames.Text == String.Empty)
                {
                    MessageBox.Show("Please import an Excel file", "Scheduling Assistant", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    pictureBox4.Visible = false;
                }
                else if (dataGridView.RowCount == 0)
                {
                    MessageBox.Show("Please import an Excel file", "Scheduling Assistant", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    saveFileDialog.InitialDirectory = "D";
                    saveFileDialog.Title = "Save an Excel File";
                    saveFileDialog.FileName = "MSACS";
                    saveFileDialog.Filter = "Excel Files(2013)|*xlsx";
                    Excel.Worksheet ws = excelSheet.Worksheets[ExcelSheetNames.SelectedIndex + 1];

					if (saveFileDialog.ShowDialog() != DialogResult.Cancel)
					{
						foreach (Int32 i in rowmarkedasdeleted)
						{
							Excel.Range range = ws.get_Range("A1", "Z1");
							range.get_Range("A" + (i + 2), "Z" + (i + 2)).Font.Strikethrough = true;
						}
						Excel.Range r = ws.UsedRange;
						int rows = ws.Range["A1"].Offset[ws.Rows.Count - 1, 0].End[Excel.XlDirection.xlUp].Row;
						r.get_Range("A" + (1), "A" + (rows)).HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;

						ws.SaveAs(saveFileDialog.FileName.ToString().Contains(".xlsx") ? saveFileDialog.FileName.ToString() : saveFileDialog.FileName.ToString() + ".xlsx");
						GC.Collect();
						excelSheet.Quit();
						MessageBox.Show("File has been exported succesfully.", "Export", MessageBoxButtons.Ok, MessageBoxIcon.Information);
					}
					else
					{
						pictureBox4.Visible = false;
					}
                }
                pictureBox4.Visible = false;
            }
            catch (Exception ex)
            {
                string str = ex.StackTrace;
                Console.WriteLine(str);
            }
        }
               
        private void Add_Click(object sender, EventArgs e) {
            try
            {
                dataTbl.Rows.Add();               
                int counter = dataTbl.Rows.Count;
                for (int i = 1; i <= counter; ++i)
                {
                    dataTbl.Rows[i - 1]["SNO"] = i;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                string str = ex.StackTrace;
                Console.WriteLine(str);
            }
        }

        private void Update_Click(object sender, EventArgs e) {
            try
            {
                var distinctInstructor = dataTbl.AsEnumerable().Select(x => x.Field<double?>("INSTRUCTOR_ID") == null ? "" : Convert.ToString(x.Field<double?>("INSTRUCTOR_ID"))).ToList();
                //var distinctInstructor = dataTbl.AsEnumerable().Select(x => Convert.ToString(x.Field<double>("INSTRUCTOR_ID"))).ToList();
                distinctInstructor = distinctInstructor.Where(x => !string.IsNullOrEmpty(x)).Distinct().ToList();

                int intdataValidation = distinctInstructor.Where(x => x.Length < 9 || x.Length > 9).Count();

                if (intdataValidation > 0)
                {
                    DisposPI();
                    MessageBox.Show("Data is invalid", "Scheduling Assistant", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                int count = listchanged.Count;
                string result = "";
                button1.Enabled = true;
                cacheDataStructures();
                if (listchanged.Count != 0)
                {
                    updatetrial();
                    foreach (ChangedCellvalue cv in listchanged)
                    {
                        dataTbl.Rows[cv.ROWINDEX].ItemArray[cv.COLINDEX] = dataGridView.Rows[cv.ROWINDEX].Cells[cv.COLINDEX].Value.ToString();
                        Console.WriteLine(cv.ToString());
                        Console.WriteLine(dataTbl.Rows[cv.ROWINDEX].ItemArray[cv.COLINDEX]);
                    }
                    BindingSource bd = new BindingSource();
                    bd.DataSource = dataTbl;
                    dataGridView.DataSource = bd;

                    tryconflictconflict(dataGridView);
                    foreach (Int32 i in rowmarkedasdeleted)
                    {
                        dataGridView.Rows[i].DefaultCellStyle.Font = new System.Drawing.Font("Calibri", 11.23F, FontStyle.Strikeout);
                    }
                    foreach (ChangedCellvalue cv in listchanged)
                    {
                        result = "At Row " + (cv.ROWINDEX + 1).ToString() + " and Column " + (cv.COLINDEX + 1).ToString() + Environment.NewLine + cv.Name.ToString() + " has been updated " + "to " + cv.Value.ToString() + Environment.NewLine + result + Environment.NewLine;
                    }
                    MessageBox.Show("Updated Successfully", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    
                }
            }
            catch (Exception exx)
            {
                String str = exx.StackTrace;
                Console.WriteLine(str);
            }
            listchanged.Clear();
        }

        private void cacheDataStructures()
        {
            crn.Clear();
            section.Clear();
            days.Clear();
            timeList.Clear();
            locationList.Clear();
            instID.Clear();

            foreach(DataRow row in dataTbl.Rows){
                var crn_col_index = row.Table.Columns["CRN"].Ordinal;
                var section_col_index=row.Table.Columns["SECTION"].Ordinal;
                var days_col_index = row.Table.Columns["DAYS"].Ordinal;
                var time_col_index = row.Table.Columns["TIME"].Ordinal;
                var location_col_index=row.Table.Columns["LOCATION"].Ordinal;
                var instructorId_col_index = row.Table.Columns["INSTRUCTOR ID"].Ordinal;

                crn.Add(row.ItemArray[crn_col_index].ToString().Trim());
                section.Add(row.ItemArray[section_col_index].ToString().Trim());
                days.Add(row.ItemArray[days_col_index].ToString().Trim());
                timeList.Add(row.ItemArray[time_col_index].ToString().Trim());
                locationList.Add(row.ItemArray[location_col_index].ToString().Trim());
                instID.Add(row.ItemArray[instructorId_col_index].ToString().Trim());
            }
        }
        
        private void Delete_Click(object sender, EventArgs e) {
            try
            {
                if (dataGridView.SelectedCells.Count >= 1)
                {
                    DialogResult result = MessageBox.Show("Are you sure you want to delete record", "Class Scheduling", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == DialogResult.Yes)
                    {
                        dataGridView.Rows[dataGridView.CurrentCell.RowIndex].DefaultCellStyle.Font = new System.Drawing.Font("Calibri", 11.23F, FontStyle.Strikeout);
                        rowmarkedasdeleted.Add(dataGridView.CurrentCell.RowIndex);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e) {
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(this.pictureBox1, "Path of the Excel file that you have selected.");
        }

        private void pictureBox2_Click(object sender, EventArgs e) {
            System.Windows.Forms.ToolTip ToolTip2 = new System.Windows.Forms.ToolTip();
            ToolTip2.SetToolTip(this.pictureBox2, "Name of the Excel Worksheet.");
        }

        //Do not remove the below lines of code
        private void label1_Click(object sender, EventArgs e) {
        }
        private void pictureBox3_Click(object sender, EventArgs e) {
        }
        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e) {
        }
        
        private void dataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            string valuechanged="";
            string namevalue = dataGridView.Columns[e.ColumnIndex].Name; ;
            string cellValue = dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            if (namevalue == "CRN")
            {
                try
                {
                    Double crnadd = Double.Parse(dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                    listchanged.Add(new ChangedCellvalue(e.ColumnIndex, e.RowIndex, crnadd.ToString(), namevalue));
                    flag = true;
                }
                catch (Exception ex)
                {
                    string str = ex.StackTrace;
                    Console.WriteLine(str);
                    MessageBox.Show(dataGridView.Columns[e.ColumnIndex].Name + " is inappropriate value for the cell");
                    flag = false;
                }
            }else if(namevalue == "CRSE#"){
                int newInteger1;
                try
                {
                    int courseNum = Int32.Parse(dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                    if (int.TryParse(dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(), out newInteger1) || newInteger1 > 0)
                    {
                        listchanged.Add(new ChangedCellvalue(e.ColumnIndex, e.RowIndex, courseNum.ToString(), namevalue));
                    }
                    if (Convert.ToInt32(dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()) < 0)
                    {
                        dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
                    }
                }
                catch (Exception exc)
                {
                    DataGridView1_DataError(null, null);
                }                
            }
            
            else if(namevalue == "SECTION"){
                int newInteger;
                try
                {
                    int secNums = Int32.Parse(dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                    if (int.TryParse(dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(), out newInteger) || newInteger > 0)
                    {
                        listchanged.Add(new ChangedCellvalue(e.ColumnIndex, e.RowIndex, secNums.ToString(), namevalue));
                    }
                    if(Convert.ToInt32(dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()) < 0){
                        dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
                    }
                }
                catch (Exception exc)
                {
                    DataGridView1_DataError(null, null);
                }
            }
           else  if (dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == null) {
                listchanged.Add(new ChangedCellvalue(e.ColumnIndex, e.RowIndex, valuechanged.ToString(),namevalue));
            }
            else
            { 
                valuechanged = dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                namevalue = dataGridView.Columns[e.ColumnIndex].Name;
                listchanged.Add(new ChangedCellvalue(e.ColumnIndex, e.RowIndex, valuechanged.ToString(),namevalue));
            }
           myvalue = listchanged.ToArray();
           Console.WriteLine(e.ColumnIndex);
           Console.WriteLine(e.RowIndex);
           Console.WriteLine(dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
        }

        //Validation a cell in Section Column
        private void dataGrid1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            dataGridView.CancelEdit();
            e.Cancel = true;
        }

        //Handling default Data Error Event for Section Column
        private void DataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs anError)
        {
            try
            {
                if (Convert.ToInt32(dataGridView.Rows[anError.RowIndex].Cells[anError.ColumnIndex].Value.ToString()) < 0)
                {
                    MessageBox.Show("Value cannot be a negative number", "Incorrect Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    dataGridView.Rows[anError.RowIndex].Cells[anError.ColumnIndex].Value = Clipboard.GetText();
                    anError.ThrowException = false;
                }
                else
                {
                    anError.ThrowException = false;
                }
                anError.ThrowException = false;
                MessageBox.Show("Only positive Integer values are allowed", "Illegal value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch(NullReferenceException ne){
                
               MessageBox.Show("No value has been assigned", "Empty Cell", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception e)
            {
                anError.ThrowException = false;
            }
        }

        //Creating a Help tool strip
        private void helpToolStripMenuItem_Click(object sender, EventArgs e) {
            
        }

        private void userManualToolStripMenuItem_Click(object sender, EventArgs e) {
            try
            {
                Help.ShowHelp(this, "file://C:\\Users\\s521650\\Desktop\\Scheduling Assistants version 11\\Help Viewer Files\\Scheduling Assistants.chm");
            }
            catch (Exception exc)
            {
                String str = exc.StackTrace;
                Console.WriteLine(str);
            }
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e) {
            Export_File_Click(null, new EventArgs());
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            checkConflicts_Click(null, new EventArgs());
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Process[] allExclProc = Process.GetProcessesByName("EXCEL");
                foreach (Process allProcID in allExclProc)
                {
                    int procsID = allProcID.Id;
                    Process.GetProcessById(procsID).Kill();
                }
                  Application.Exit();
                Clipboard.Clear();
            }
            catch (Exception exx)
            {
                String str = exx.StackTrace;
                Console.WriteLine(str);
            }
        }

        private void rowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Add_Click(null, new EventArgs());
            }
            catch (Exception exc)
            {
                String str = exc.StackTrace;
                Console.WriteLine(str);
            }
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView.RowCount >= 1)
                {
                    if (dataGridView.SelectedCells.Count >= 1)
                    {
                        int rowIndx = dataGridView.CurrentCell.RowIndex;
                        int colIndx = dataGridView.CurrentCell.ColumnIndex;
                        if ((dataGridView[colIndx, rowIndx].ToString().Length) > 0)
                        {
                            string data = (dataGridView[colIndx, rowIndx].Value).ToString();
                            Clipboard.Clear();
                            Clipboard.SetText(data);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                String str = exc.StackTrace;
                Console.WriteLine(str);
            }          
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Choose_File_Click(null, new EventArgs());
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Export_File_Click(null, new EventArgs());
        }

        private void clearAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView.RowCount >= 1)
                {
                    try
                    {
                        dataGridView.DataSource = null;
                        dataTbl.Clear();
                        dataGridView.Columns.Clear();
                        dataTbl.Columns.Clear();
                    }
                    catch (System.ArgumentException)
                    {
                        MessageBox.Show("Cannot be cleared now", "Clear All", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch (Exception exc)
            {
                String str = exc.StackTrace;
                Console.WriteLine(str);
            }
         }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView.RowCount >= 1)
                {
                    if (dataGridView.SelectedCells.Count == 1)
                    {
                        int rowIndx = dataGridView.CurrentCell.RowIndex;
                        int colIndx = dataGridView.CurrentCell.ColumnIndex;
                        if ((dataGridView[colIndx, rowIndx].ToString().Length) > 0)
                        {
                            string data = (dataGridView[colIndx, rowIndx].Value).ToString();
                            dataGridView[colIndx, rowIndx].Value = "";
                            Clipboard.SetText(data);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Only a single cell can be removed and pasted", "Cut Operation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch (Exception exc)
            {
                String str = exc.StackTrace;
                Console.WriteLine(str);
            }          
        }

        private void pasteToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView.RowCount >= 1)
                {
                    if (dataGridView.SelectedCells.Count > 0)
                    {
                        if (Clipboard.GetText().Length > 0)
                        {
                            string dataToBePasted = Clipboard.GetText();
                            int rowIndx = dataGridView.CurrentCell.RowIndex;
                            int colIndx = dataGridView.CurrentCell.ColumnIndex;
                            dataGridView[colIndx, rowIndx].Value = dataToBePasted;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                String str = exc.StackTrace;
                Console.WriteLine(str);
            }
        }

        private void SchedulingAssistant_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Update.Enabled = true;
                Export_File.Enabled = true;
                IEnumerable<string> time = getTime();
                IEnumerable<string> locationarray = getLocation();
                IEnumerable<string> instructorID = getInstructor();
                IEnumerable<string> Section = getSection();
                IEnumerable<string> days = getDays();
                IEnumerable<string> crns = getCrn();
                storeTime(time, excelSheet);
                storeLocation(locationarray, excelSheet);
                storeInstructor(instructorID, excelSheet);
                storeSection(Section, excelSheet);
                storeDays(days, excelSheet);
                storeCrn(crns, excelSheet);
                if (listchanged.Count != 0)
                {
                    foreach (ChangedCellvalue cv in listchanged)
                    {
                        dataTbl.Rows[cv.ROWINDEX].ItemArray[cv.COLINDEX] = dataGridView.Rows[cv.ROWINDEX].Cells[cv.COLINDEX].Value.ToString();
                        Console.WriteLine(dataTbl.ToString());
                        dataGridView.DataSource = dataTbl;
                    }
                    dataGridView.RefreshEdit();
                    tryconflictconflict(dataGridView);
                }
                else
                {
                    tryconflictconflict(dataGridView);
                }
            }
            catch (Exception exx)
            {
                String str = exx.StackTrace;
                Console.WriteLine(str);
            }
            button1.Enabled = false;
            MessageBox.Show("Conflict Check has been completed", "Conflict Check", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private IEnumerable<string> getTime()
        {
            IEnumerable<string> tm;
            try
            {
                tm = dataTbl.AsEnumerable().Select(x => x.Field<string>("TIME"));
                // tm = (from ms in excelQF.Worksheet<MovieSheet>(ExcelSheetNames.SelectedIndex) select ms.TIME).AsEnumerable().ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
            return tm;
        }
        //Location to list
        private IEnumerable<string> getLocation()
        {
            IEnumerable<string> lo;
            try
            {
                 lo = dataTbl.AsEnumerable().Select(x => x.Field<string>("LOCATION"));
                //lo = (from ms in excelQF.Worksheet<MovieSheet>(ExcelSheetNames.SelectedIndex) select ms.LOCATION).AsEnumerable().ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
            return lo;
        }

        //Time to list
        List<String> timeList;
        private void storeTime(IEnumerable<string> timel, Microsoft.Office.Interop.Excel.Application excel)
        {
            timeList = timel.ToList();
        }
        // location to list
        List<String> locationList;
        private void storeLocation(IEnumerable<string> lList, Microsoft.Office.Interop.Excel.Application excel)
        {
            locationList = lList.ToList();
        }
        //instructor id
        private IEnumerable<string> getInstructor()
        {
            IEnumerable<string> id;
            try
            {
                  id = dataTbl.AsEnumerable().Select(x => Convert.ToString(x.Field<double>("INSTRUCTOR_ID")));
                //id = (from ms in excelQF.Worksheet<MovieSheet>(ExcelSheetNames.SelectedIndex) select ms.INSTRUCTOR_ID).AsEnumerable().ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
            return id;
        }
        private IEnumerable<string> getSection()
        {
            IEnumerable<string> sec;
            try
            {
                sec = (from ms in excelQF.Worksheet<MovieSheet>(ExcelSheetNames.SelectedIndex) select ms.SECTION).AsEnumerable().ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
            return sec;
        }
        private IEnumerable<string> getDays()
        {
            IEnumerable<string> days;
            try
            {
                 days = dataTbl.AsEnumerable().Select(x => x.Field<string>("DAYS"));
               // days = (from ms in excelQF.Worksheet<MovieSheet>(ExcelSheetNames.SelectedIndex) select ms.DAYS).AsEnumerable().ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
            return days;
        }
        private IEnumerable<string> getCrn()
        {
            IEnumerable<string> crn;
            try
            {
               crn = dataTbl.AsEnumerable().Select(x => x.Field<string>("CRN"));
                //crn = (from ms in excelQF.Worksheet<MovieSheet>(ExcelSheetNames.SelectedIndex) select ms.CRN).AsEnumerable().ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
            return crn;
        }

        //instructor to list
        List<String> instID;
        private void storeInstructor(IEnumerable<string> idList, Microsoft.Office.Interop.Excel.Application excel)
        {
            instID = idList.ToList();
        }
        //SECTION to list
        List<String> section;
        private void storeSection(IEnumerable<string> idList, Microsoft.Office.Interop.Excel.Application excel)
        {
            section = idList.ToList();
        }
        //DAYS to list
        List<String> days;
        private void storeDays(IEnumerable<string> idList, Microsoft.Office.Interop.Excel.Application excel)
        {
            days = idList.ToList();
        }
        //CRN to list
        List<String> crn;
        private void storeCrn(IEnumerable<string> idList, Microsoft.Office.Interop.Excel.Application excel)
        {
            crn = idList.ToList();
        }
        //Testing
        public Microsoft.Office.Interop.Excel.Application updatetrial()
        {
                foreach (ChangedCellvalue cv in listchanged)
                {
                    if (excelSheet.Worksheets[ExcelSheetNames.SelectedIndex  + 1].Cells[cv.ROWINDEX + 2, cv.COLINDEX + 1].Value2 != null && dataGridView.Rows[cv.ROWINDEX].Cells[cv.COLINDEX].Value == null)
                    {
                        string str1 = excelSheet.Worksheets[ExcelSheetNames.SelectedIndex + 1].Cells[cv.ROWINDEX + 2, cv.COLINDEX + 1].Value.ToString();
                        int strlength1 = str1.Length;
                        Excel.Range range = excelSheet.Worksheets[2].Cells[cv.ROWINDEX + 2, cv.COLINDEX + 1];
                        range.get_Characters(1, strlength1).Font.Strikethrough = true;

                    }
                    else if ((excelSheet.Worksheets[ExcelSheetNames.SelectedIndex +1].Cells[cv.ROWINDEX + 2, cv.COLINDEX + 1].Value2 == null) && dataGridView.Rows[cv.ROWINDEX].Cells[cv.COLINDEX].Value != null)
                    {
                        excelSheet.Worksheets[ExcelSheetNames.SelectedIndex + 1].Cells[cv.ROWINDEX + 2, cv.COLINDEX + 1] = dataGridView.Rows[cv.ROWINDEX].Cells[cv.COLINDEX].Value.ToString();
                        excelSheet.Worksheets[ExcelSheetNames.SelectedIndex +1].Cells[cv.ROWINDEX + 2, cv.COLINDEX + 1].Font.Color = System.Drawing.Color.Red;
                    }
                    else if (excelSheet.Worksheets[ExcelSheetNames.SelectedIndex +1].Cells[cv.ROWINDEX + 2, cv.COLINDEX + 1].Value2 == null && dataGridView.Rows[cv.ROWINDEX].Cells[cv.COLINDEX].Value == null)
                    {

                    }
                    else
                    {
                        if ((excelSheet.Worksheets[ExcelSheetNames.SelectedIndex +1].Cells[cv.ROWINDEX + 2, cv.COLINDEX + 1].Value.ToString()).Equals(dataGridView.Rows[cv.ROWINDEX].Cells[cv.COLINDEX].Value.ToString()))
                        {
                        }
                        else
                        {
                            string str = excelSheet.Cells[cv.ROWINDEX + 2, cv.COLINDEX + 1].Value.ToString();
                            string str1 = dataGridView.Rows[cv.ROWINDEX].Cells[cv.COLINDEX].Value.ToString();
                            //int index = str1.IndexOf(dataGridView.Rows[cv.ROWINDEX].Cells[cv.COLINDEX].Value.ToString());
                            int strlength = str.Length;
                            int strlength3 = ("\n").Length;
                            int strlength1 = str1.Length;
                            excelSheet.Worksheets[ExcelSheetNames.SelectedIndex + 1].Cells[cv.ROWINDEX + 2, cv.COLINDEX + 1] = dataGridView.Rows[cv.ROWINDEX].Cells[cv.COLINDEX].Value.ToString() + "\n" + excelSheet.Cells[cv.ROWINDEX + 2, cv.COLINDEX + 1].Value.ToString();
                            Excel.Range range = excelSheet.Worksheets[ExcelSheetNames.SelectedIndex +1].Cells[cv.ROWINDEX + 2, cv.COLINDEX + 1];
                            range.get_Characters(1, strlength1 + strlength3).Font.Color = System.Drawing.Color.Red;
                            range.get_Characters(strlength1 + strlength3, strlength1 + strlength3 + strlength).Font.Strikethrough = true;
                            dataTbl.Rows[cv.ROWINDEX].ItemArray[cv.COLINDEX] = dataGridView.Rows[cv.ROWINDEX].Cells[cv.COLINDEX].Value.ToString();
                            Console.WriteLine(dataTbl.Rows[cv.ROWINDEX].ItemArray[cv.COLINDEX].ToString());
                        }

                    }
                }
                return excelSheet;
        }

        //Testing Start
        private void tryconflictconflict(DataGridView dataGridView)
        {
            try
            {
                //int rows = excelSheet.Range["A1"].Offset[excelSheet.Rows.Count - 1, 0].End[Excel.XlDirection.xlUp].Row;
                int rows = dataTbl.Rows.Count;
                for (int i = 0; i < rows - 1; i++)
                {
                    for (int j = 0; j < rows - 1; j++)
                    {
                        dataGridView.DataSource = dataTbl;
                        if (!(timeList[i].Equals("-")) && (!(timeList[j].Equals("-"))) && (!(timeList[i].Equals("              "))) && (!(timeList[j].Equals("                    "))) && (!(timeList[i].Equals("0"))) && (!(timeList[j].Equals("0"))))
                        {
                            string st = timeList[i];
                            string ans = st.Substring(st.LastIndexOf('-') + 1);
                            string first = ans.Substring(0, 2);
                            string n = st.Substring(0, 4);
                            string st1 = timeList[j];
                            string ans1 = st1.Substring(0, 4);
                            string ans2 = st1.Substring(st.LastIndexOf('-') + 1);
                            string second = ans2.Substring(0, 2);
                            int i1 = int.Parse(ans);
                            int j1 = int.Parse(ans1);
                            int i2 = int.Parse(first);
                            int j2 = int.Parse(second);
                            int k1 = int.Parse(n);
                            int k2 = int.Parse(ans2);                            
                            if (i != j)
                            {
                                if ((timeList[i].Equals(timeList[j])) && (locationList[i].Equals(locationList[j])) && (instID[i].Equals(instID[j]))
                                 && (days[i].Equals(days[j])) && (crn[i].Equals(crn[j])))
                                {
                                    if (!(section[i].Equals(section[j])))
                                    {
                                        dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.LawnGreen;
                                        Console.WriteLine(crn[i].ToString());
                                    }
                                }
                                else if ((timeList[i].Equals(timeList[j])) && (instID[i].Equals(instID[j]))
                            && (section[i].Equals(section[j])) && (days[i].Equals(days[j])) && (crn[i].Equals(crn[j])))
                                {
                                    if (!(locationList[i].Equals(locationList[j])))
                                    {
                                        dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.Lime;
                                        Console.WriteLine(crn[i].ToString());
                                    }
                                }
                                else if ((locationList[i].Equals(locationList[j])) && (instID[i].Equals(instID[j]))
                               && (section[i].Equals(section[j])) && (days[i].Equals(days[j])) && (crn[i].Equals(crn[j])))
                                {
                                    if (!(timeList[i].Equals(timeList[j])))
                                    {
                                        dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.Gray;
                                        Console.WriteLine(crn[i].ToString());
                                    }
                                }
                                else if ((locationList[i].Equals(locationList[j])) && (timeList[i].Equals(timeList[j]))
                              && (section[i].Equals(section[j])) && (days[i].Equals(days[j])) && (crn[i].Equals(crn[j])))
                                {
                                    if (!(instID[i].Equals(instID[j])))
                                    {
                                        dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.LemonChiffon;
                                        Console.WriteLine(crn[i].ToString());
                                    }
                                }
                                else if ((locationList[i].Equals(locationList[j])) && (timeList[i].Equals(timeList[j]))
                              && !(section[i].Equals(section[j])) && (days[i].Equals(days[j])) && (crn[i].Equals(crn[j])))
                                {
                                    if (!(instID[i].Equals(instID[j])))
                                    {
                                        dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.LightCyan;
                                        Console.WriteLine(crn[i].ToString());
                                    }
                                }
                                else if (!(locationList[i].Equals(locationList[j])) && (timeList[i].Equals(timeList[j]))
                              && (section[i].Equals(section[j])) && (days[i].Equals(days[j])) && (crn[i].Equals(crn[j])))
                                {
                                    if (!(instID[i].Equals(instID[j])))
                                    {
                                        dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.LightCoral;
                                        Console.WriteLine(crn[i].ToString());
                                    }
                                }
                                else if ((locationList[i].Equals(locationList[j])) && !(timeList[i].Equals(timeList[j]))
                              && (section[i].Equals(section[j])) && (days[i].Equals(days[j])) && (crn[i].Equals(crn[j])))
                                {
                                    if (!(instID[i].Equals(instID[j])))
                                    {
                                        dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.LightGoldenrodYellow;
                                        Console.WriteLine(crn[i].ToString());
                                    }
                                }
                                else if (!(locationList[i].Equals(locationList[j])) && !(timeList[i].Equals(timeList[j]))
                              && (section[i].Equals(section[j])) && (days[i].Equals(days[j])) && (crn[i].Equals(crn[j])))
                                {
                                    if (!(instID[i].Equals(instID[j])))
                                    {
                                        dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.LightSalmon;
                                        Console.WriteLine(crn[i].ToString());
                                    }
                                }
                                else if ((locationList[i].Equals(locationList[j])) && (timeList[i].Equals(timeList[j]))
                               && (days[i].Equals(days[j])) && (instID[i].Equals(instID[j])))
                                {
                                    if (!(crn[i].Equals(crn[j])))
                                    {
                                        dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.MediumAquamarine;
                                        Console.WriteLine(crn[i].ToString());
                                    }
                                }
                                else if (!(locationList[i].Equals(locationList[j])) && (timeList[i].Equals(timeList[j]))
                              && (days[i].Equals(days[j])) && (instID[i].Equals(instID[j])))
                                {
                                    if (!(crn[i].Equals(crn[j])))
                                    {
                                        dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.LightSkyBlue;
                                        Console.WriteLine(crn[i].ToString());
                                    }
                                }
                                
                                else if ((locationList[i].Equals(locationList[j])) && (days[i].Equals(days[j])) && !(timeList[i].Equals(timeList[j])) && !(instID[i].Equals(instID[j])) && !(crn[i].Equals(crn[j])))
                                {
                                    if ((i1 >= j1) && (i2 >= j2) && !(k1 - k2 >= 5))
                                    {
                                        dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.LightSlateGray;
                                    }
                                }
                                else if ((locationList[i].Equals(locationList[j])) && (timeList[i].Equals(timeList[j])) && !(instID[i].Equals(instID[j])) && !(crn[i].Equals(crn[j])) && !(days[i].Equals(days[j])))
                                {
                                    if ((days[i].Contains("MW") && days[j].Contains("MW")) || (days[i].Contains("WF") && days[j].Contains("WF")) || (days[i].Contains("MF") && days[j].Contains("MF")) || (days[i].Contains("MT") && days[j].Contains("MT")) || (days[i].Contains("MR") && days[j].Contains("MR")) || (days[i].Contains("TW") && days[j].Contains("TW"))
                                       || (days[i].Contains("TR") && days[j].Contains("TR")) || (days[i].Contains("TF") && days[j].Contains("TF")) || (days[i].Contains("WR") && days[j].Contains("WR")) || (days[i].Contains("RF") && days[j].Contains("RF")))
                                    {
                                        if ((days[i].Contains("F") || days[j].Contains("F")) || ((days[i].Contains("M") || days[j].Contains("M"))) || (days[i].Contains("W") && days[j].Contains("W")) || (days[i].Contains("T") && days[j].Contains("T")) || (days[i].Contains("R") && days[j].Contains("R")))
                                        {
                                            dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.Bisque;
                                        }
                                    }
                                }
                                else if ((locationList[i].Equals(locationList[j])) && (timeList[i].Equals(timeList[j]))
                               && (days[i].Equals(days[j])) && !(instID[i].Equals(instID[j])))
                                {
                                    if (!(crn[i].Equals(crn[j])))
                                    {
                                        dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                                        Console.WriteLine(crn[i].ToString());
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exx)
            {
                String str = exx.StackTrace;
                Console.WriteLine(str);
            }
            dataGridView.EndEdit();
            dataGridView.Refresh();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView.RowCount >= 1)
                {
                    updatetrial();
                    MessageBox.Show("Updated successfully", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception exc)
            {
                String str = exc.StackTrace;
                Console.WriteLine(str);
            }
        }

        
        private void conflictCheckToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Update.Enabled = true;
                button1.Enabled = false;
                IEnumerable<string> time = getTime();
                IEnumerable<string> locationarray = getLocation();
                IEnumerable<string> instructorID = getInstructor();
                IEnumerable<string> Section = getSection();
                IEnumerable<string> days = getDays();
                IEnumerable<string> crns = getCrn();
                storeTime(time, excelSheet);
                storeLocation(locationarray, excelSheet);
                storeInstructor(instructorID, excelSheet);
                storeSection(Section, excelSheet);
                storeDays(days, excelSheet);
                storeCrn(crns, excelSheet);
                if (listchanged.Count != 0)
                {
                    foreach (ChangedCellvalue cv in listchanged)
                    {
                        dataTbl.Rows[cv.ROWINDEX].ItemArray[cv.COLINDEX] = dataGridView.Rows[cv.ROWINDEX].Cells[cv.COLINDEX].Value.ToString();
                        Console.WriteLine(dataTbl.ToString());
                        dataGridView.DataSource = dataTbl;
                    }
                    dataGridView.RefreshEdit();
                    tryconflictconflict(dataGridView);
                }
                else
                {
                    tryconflictconflict(dataGridView);
                }
            }
            catch (Exception exc)
            {
                String str = exc.StackTrace;
                Console.WriteLine(str);
            }
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView.RowCount > 0)
                {
                    dataGridView.SelectAll();
                }
            }
            catch (Exception exc)
            {
                String str = exc.StackTrace;
                Console.WriteLine(str);
            }
        }

        protected virtual void OnClosin(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBox.Show("Closed");
        }
      
    }
}

private void button3_Click(object sender, EventArgs e)
{
    saveFileDialog.InitialDirectory = "D";
    saveFileDialog.Title = "Save a PDF File";
    saveFileDialog.FileName = "Instructor";
    saveFileDialog.Filter = "Pdf Files|*.pdf";

    if (saveFileDialog.ShowDialog() == DialogResult.OK)
    {
        // var distinctInstructor = dataTbl.AsEnumerable().Select(x => Convert.ToString(x.Field<double>("INSTRUCTOR_ID"))).ToList();
        var distinctInstructor = dataTbl.AsEnumerable().Select(x => x.Field<double?>("INSTRUCTOR_ID") == null ? "" : Convert.ToString(x.Field<double?>("INSTRUCTOR_ID"))).ToList();
        distinctInstructor = distinctInstructor.Distinct().ToList();
        SchedulingAssistantHelper sch = new SchedulingAssistantHelper(saveFileDialog.FileName.ToString().Contains(".pdf") ? saveFileDialog.FileName.ToString() : saveFileDialog.FileName.ToString() + ".pdf");
        sch.MakeDataTable(dataTbl, "Instructor", new List<int>(), distinctInstructor);
    }
}

private void button2_Click(object sender, EventArgs e)
{
    saveFileDialog.InitialDirectory = "D";
    saveFileDialog.Title = "Save a PDF File";
    saveFileDialog.FileName = "Rooms";
    saveFileDialog.Filter = "Pdf Files|*.pdf";

    if (saveFileDialog.ShowDialog() == DialogResult.OK)
    {
        var distinctInstructor = dataTbl.AsEnumerable().Select(x => Convert.ToString(x.Field<string>("Location"))).ToList();
        distinctInstructor = distinctInstructor.Distinct().ToList();
        SchedulingAssistantHelper sch = new SchedulingAssistantHelper(saveFileDialog.FileName.ToString().Contains(".pdf") ? saveFileDialog.FileName.ToString() : saveFileDialog.FileName.ToString() + ".pdf");
        sch.MakeDataTable(dataTbl, "Room", new List<int>(), distinctInstructor);
    }
}

public ChangedCellValue updateDataBackup(int row, int column, String oldValue, String newCellValue)
{
    try
    {
        ChangedCellValue updatedValues = new ChangedCellValue();
        updatedValues.row = row;
        updatedValues.column = column;
        updatedValues.oldValue = oldValue;
        updatedValues.newValue = newCellValue;
        return updatedValues;
    }
    catch (Exception ex)
    {
        MessageBox.Show(ex.Message);
        string str = ex.StackTrace;
        Console.WriteLine(str);
        return null;
    }
}



public struct MovieSheet
{
    public string TIME { get; set; }
    public string LOCATION { get; set; }
    public string INSTRUCTOR_ID { get; set; }
    public string CRN { get; set; }
    public string DAYS { get; set; }
    public string SECTION { get; set; }
}
public struct ChangedCellvalue
{
    public Int32 COLINDEX;
    public Int32 ROWINDEX;
    public string Value;
    public string Name;
    public ChangedCellvalue(Int32 colindex, Int32 rowindex, string valuechanged, string changedname)
    {
        COLINDEX = colindex;
        ROWINDEX = rowindex;
        Value = valuechanged;
        Name = changedname;
    }
}

